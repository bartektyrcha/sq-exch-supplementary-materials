# sq-exch-supplementary-materials


## Geometries

The folder "geometries" contains subfolders for each of the test systems. Each of the subfolders contain geometry files for a given system.


## Numerical results

The folder `results` contains csv tables with results of all SAPT exchange corrections calculated for this work.

The file `ar-h2o.csv` contains results of calculations for Ar-H2O system.

The file `be-h2o.csv` contains results of calculations for Be-H2O system.

The file `be-nh3.csv` contains results of calculations for Be-NH3 system.

The file `c2h4-c2h4.csv` contains results of calculations for system 
of ethylene dimer (C2H4-C2H4).

The file `mg-h2o.csv` contains results of calculations for Mg-H2O system.

Each file contains the same columns. 
`R/Re` are intermolecular separations in multiple of the equlibrium distances presented in Table 1. of the main text. 
For exchange corrections, column names accopanied by `_S2` or `_S4` referr to corrections calculated in $S^2$ and $S^4$ approximations respectively, 
`_Sinf` referr to corrections calculated within the newly derived second-quantised approach, whereas no suffix referr to corrections calculated with density-matrix approach -- our reference. 
`Diff_` prefix indicates difference of a given correction with respect to the reference. 
`RelErr_` indicates relative errors and `Ratio_` means ration of a given 
correction with respect to the reference value. 
`Total_` prefix indicates sum of `Exch100`, `ExchInd200r` and `ExchDisp200`.
